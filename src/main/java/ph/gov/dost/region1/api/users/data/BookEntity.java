package ph.gov.dost.region1.api.users.data;

import javax.persistence.*;
import java.time.LocalDateTime;

import static javax.persistence.GenerationType.SEQUENCE;

/**
 * Created on 1/24/21
 * Created by Ryan Jan Borja
 * rrborja@region1.dost.gov.ph
 * ryanjan18@outlook.ph
 */

@Entity(name = "Book")
@Table(
  name = "book",
  uniqueConstraints = {
    @UniqueConstraint(name = "book_name_unique", columnNames = "book_name"),
//    @UniqueConstraint(name = "student_id_unique", columnNames = "student_id")
  }
)
public class BookEntity {

  @Id
  @SequenceGenerator(name = "book_sequence", sequenceName = "book_sequence", allocationSize = 1)
  @GeneratedValue(strategy = SEQUENCE, generator = "book_sequence")
  @Column(name = "id", updatable = false)
  private Long id;

  @Column(name = "book_name", nullable = false)
  private String bookName;

  @Column(name = "created_at", nullable = false, columnDefinition = "TIMESTAMP WITHOUT TIME ZONE")
  private LocalDateTime createdAt;

  @ManyToOne
  @JoinColumn(
    name = "student_id",
    nullable = false,
    referencedColumnName = "id",
    foreignKey = @ForeignKey(name = "student_book_fk")
  )
  private StudentEntity student;

  public BookEntity() { }

  public BookEntity(String bookName, LocalDateTime createdAt) {
    this.bookName = bookName;
    this.createdAt = createdAt;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getBookName() {
    return bookName;
  }

  public void setBookName(String bookName) {
    this.bookName = bookName;
  }

  public LocalDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(LocalDateTime createdAt) {
    this.createdAt = createdAt;
  }

  public StudentEntity getStudent() {
    return student;
  }

  public void setStudent(StudentEntity student) {
    this.student = student;
  }

  @Override
  public String toString() {
    return "BookEntity{" +
      "id=" + id +
      ", bookName='" + bookName + '\'' +
      ", createdAt=" + createdAt +
      ", student=" + student +
      '}';
  }

}

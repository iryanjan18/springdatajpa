package ph.gov.dost.region1.api.users;

import com.github.javafaker.Faker;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Sort;
import ph.gov.dost.region1.api.users.data.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@SpringBootApplication
public class UsersApplication {

  public static void main(String[] args) {
    SpringApplication.run(UsersApplication.class, args);
  }

  @Bean
  CommandLineRunner commandLineRunner(
    StudentRepository studentRepository) {

    return args -> {
      Faker faker = new Faker();
      String firstName = faker.name().firstName();
      String lastName = faker.name().lastName();
      String email = String.format("%s.%s@company.com", firstName, lastName);
      StudentEntity student = new StudentEntity(UUID.randomUUID(), firstName, lastName, email, faker.number().numberBetween(19, 70));

      student.addBook(new BookEntity("Clean Code", LocalDateTime.now().minusYears(1)));
      student.addBook(new BookEntity("Spring Data JPA", LocalDateTime.now()));
      student.addBook(new BookEntity("Data Structures and Algorithms", LocalDateTime.now().minusYears(5)));

      UUID card_number = UUID.randomUUID();
      StudentIdCard studentIdCard = new StudentIdCard(card_number, student);

      student.setStudentIdCard(studentIdCard);

      student.addEnrollment(
        new EnrollmentEntity(
          new EnrollmentId(1L, 1L),
          student,
          new CourseEntity("Electrical Engineering", "Engineering"),
          LocalDateTime.now())
      );

      student.addEnrollment(
        new EnrollmentEntity(
          new EnrollmentId(1L, 2L),
          student,
          new CourseEntity("Circuit Design", "Technology"),
          LocalDateTime.now())
      );

      studentRepository.save(student);

      studentRepository
        .findById(1L)
        .ifPresent(studentEntity -> {
          System.out.println("Fetch book lazy...");
          List<BookEntity> books = student.getBooks();
          books.forEach(book -> {
            System.out.println(studentEntity.getFirstName() + " borrowed " + book.getBookName());
          });
        });

      studentRepository.findById(1L).ifPresent(System.out::println);

//      studentRepository.deleteById(1L);
    };

//    return args -> {
//      generateRandomStudents(studentRepository);
////      sortingFirstnameAscending(studentRepository);
//
//      PageRequest pageRequest = PageRequest.of(0, 5, Sort.by("firstName").ascending());
//      Page<StudentEntity> page = studentRepository.findAll(pageRequest);
//      System.out.println(page);
//    };

//    return args -> {
//      Student ryan = new Student("Ryan Jan", "Borja", "ryanjan18@outlook.ph", 28);
//      Student ryanfing = new Student("Ryan Jan", "Fing", "ryanjan@fing.com", 22);
//      Student luh = new Student("Luh", "Co", "luhco@company.com", 18);
//      Student feilo = new Student("Fei Lu", "Kah", "feilukah@company.com", 25);
//      studentRepository.saveAll(List.of(ryan, ryanfing, luh, feilo));
//
//      studentRepository
//        .findByEmail("ryanjan18@outlook.ph")
//        .ifPresentOrElse(
//          System.out::println,
//          () -> System.out.println("Student with email ryanjan18@outlook.ph was not found!")
//        );
//
//      studentRepository
//        .findByFirstNameEqualsAndAgeIsGreaterThanEqual("Ryan Jan", 18)
//        .forEach(System.out::println);
//
//      studentRepository
//        .findByFirstNameEqualsAndAgeIsGreaterThanEqualNative("Ryan Jan", 24)
//        .forEach(System.out::println);
//
//      System.out.println(studentRepository.deleteStudentById(4L));
//    };
  }

  private void sortingFirstnameAscending(StudentRepository studentRepository) {
//    Sort sortFirstnameAsc = Sort.by(Sort.Direction.ASC, "firstName");
    Sort sortFirstnameAsc = Sort.by("firstName").ascending().and(Sort.by("age").ascending());
    studentRepository.findAll(sortFirstnameAsc).forEach(student -> System.out.println(student.getFirstName() + " " + student.getAge()));
  }

  private void generateRandomStudents(StudentRepository studentRepository) {
    Faker faker = new Faker();
    for (int i = 0; i < 20; i++) {
      String firstName = faker.name().firstName();
      String lastName = faker.name().lastName();
      String email = String.format("%s.%s@company.com", firstName, lastName);
      StudentEntity student = new StudentEntity(UUID.randomUUID(), firstName, lastName, email, faker.number().numberBetween(19, 70));
      studentRepository.save(student);
    }
  }

}

package ph.gov.dost.region1.api.users.data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static javax.persistence.GenerationType.SEQUENCE;

/**
 * Created on 1/15/21
 * Created by Ryan Jan Borja
 * rrborja@region1.dost.gov.ph
 * ryanjan18@outlook.ph
 */

@Entity(name = "Student")
@Table(
  name = "student",
  uniqueConstraints = {
    @UniqueConstraint(name = "student_email_unique", columnNames = "email")
  }
)
public class StudentEntity {

  @Id
  @SequenceGenerator(name = "student_sequence", sequenceName = "student_sequence", allocationSize = 1)
  @GeneratedValue(strategy = SEQUENCE, generator = "student_sequence")
  @Column(name = "id", updatable = false)
  private Long id;

  @Column(name = "uid", unique = true, updatable = false, nullable = false)
  private UUID uid;

  @Column(name = "first_name", nullable = false, columnDefinition = "TEXT")
  private String firstName;

  @Column(name = "last_name", nullable = false, columnDefinition = "TEXT")
  private String lastName;

  @Column(name = "email", nullable = false, columnDefinition = "TEXT")
  private String email;

  @Column(name = "age", nullable = false)
  private Integer age;

  @OneToOne(mappedBy = "student", orphanRemoval = true, cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
  private StudentIdCard studentIdCard;

  @OneToMany(
    mappedBy = "student",
    orphanRemoval = true,
    cascade = {CascadeType.PERSIST, CascadeType.REMOVE},
    fetch = FetchType.EAGER
  )
  private List<BookEntity> books = new ArrayList<>();

  @OneToMany(
    cascade = {CascadeType.PERSIST, CascadeType.REMOVE},
    fetch = FetchType.LAZY,
    mappedBy = "student"
  )
  private List<EnrollmentEntity> enrollments = new ArrayList<>();

  public StudentEntity() {
  }

  public StudentEntity(UUID uid,
                       String firstName,
                       String lastName,
                       String email,
                       Integer age) {
    this.uid = uid;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.age = age;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public UUID getUid() {
    return uid;
  }

  public void setUid(UUID uid) {
    this.uid = uid;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  public List<BookEntity> getBooks() {
    return books;
  }

  public void setBook(List<BookEntity> book) {
    this.books = book;
  }

  public void addBook(BookEntity book) {
    if (!this.books.contains(book)) {
      this.books.add(book);
      book.setStudent(this);
    }
  }

  public void removeBook(BookEntity book) {
    if (this.books.contains(book)) {
      this.books.remove(book);
      book.setStudent(null);
    }
  }

  public void setStudentIdCard(StudentIdCard studentIdCard) {
    this.studentIdCard = studentIdCard;
  }

  public List<EnrollmentEntity> getEnrollment() {
    return enrollments;
  }

  public void addEnrollment(EnrollmentEntity enrollment) {
    if (!enrollments.contains(enrollment)) {
      enrollments.add(enrollment);
    }
  }

  public void removeEnrollment(EnrollmentEntity enrollment) {
    enrollments.remove(enrollment);
  }

  @Override
  public String toString() {
    return "StudentEntity{" +
      "id=" + id +
      ", uid='" + uid + '\'' +
      ", firstName='" + firstName + '\'' +
      ", lastName='" + lastName + '\'' +
      ", email='" + email + '\'' +
      ", age=" + age +
      '}';
  }

}
